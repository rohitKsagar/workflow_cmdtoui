<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <style type="text/css">
.mydiv {
    display: inline-block;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 400px;
    height: 400px;
    margin: auto;
    background-color: #f3f3f3;">
}
</style> -->

<title>Insert title here</title>
</head>
<body>
<div class="container">
<div class="jumbotron">
    <h1><center>Workflow</center></h1>  
    <jsp:include page="/Navbar.jsp"></jsp:include>     
  </div>
	<Title>Welcome to workflow</Title>
	<!-- <form name="frm"  id="frm"  action="http://localhost:8080/TaskManagementUI/workflow" > -->

<div >


	<form method="get" name="frm" id="frm"  class ="frmclass" action="http://localhost:8080/TaskManagementUI/transition">

<div class="form-group">
		<label>Your current task : </label> ${curtask}
		
<div class="col-sm-9 col-md-4">

			<label>  Select next task :</label><select class="form-control"  id="selectedRecord" name="selectedRecord">
					<c:forEach var="nextTaskList" items="${nextTaskList}">
						<option class="form-control" value="${nextTaskList.nextTask}" name="chooseTask">${nextTaskList.nextTask}</option>
				</c:forEach>
			<!-- 	<input type="button" value="End Workflow" name="EndTask" /> --><br/>
			
</select>
<input type="checkbox" name="endTask" value="endTask">End the Workflow<br/>
</div>	
<div class="form-group">
		<input type="submit" value="Submit" />
</div>	

	</div>

	</form>
	</div>
	
	</div>
</body>
</html>