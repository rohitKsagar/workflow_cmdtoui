<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.17/css/dataTables.bootstrap4.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></script>
<script src="https://cdn.datatables.net/1.10.17/js/dataTables.bootstrap4.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<div class="container">
 <div class="jumbotron">
    <h1><center>History</center></h3>      
   
  </div>

<table  class="table table-striped table-bordered" id="example" class="display"  style="width:50%">
					<thead>
						<tr>
							<th>Workflow History : </th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="c" items="${historylist}">
							<tr>
								<td><center>${c.name_}</center></td>
							</tr>
						</c:forEach>
			
					</tbody>
	</table>
</div>
</body>
</html>