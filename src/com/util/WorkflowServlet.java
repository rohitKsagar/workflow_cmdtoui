package com.util;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.flowable.TaskManagement;

/**
 * Servlet implementation class WorkflowServlet
 */
@WebServlet("/workflow")
public class WorkflowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String chooseTask = request.getParameter("chooseTask");
		System.out.println("String chooseTask = "+ chooseTask);
		RequestDispatcher rd = request.getRequestDispatcher("/WorkflowTaskMangement.jsp");  
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
