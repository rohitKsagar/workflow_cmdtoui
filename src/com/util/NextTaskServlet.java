package com.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.flowable.task.api.Task;

/**
 * Servlet implementation class NextTaskServlet
 */
@WebServlet("/nextTask")
public class NextTaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		String taskName = request.getParameter("taskName");

		
		System.out.println("next task : "+ taskName);
		PreparedStatement ps=null;
		try 
		{		LinkedHashMap<String,String> listnexttask = (LinkedHashMap) request.getSession().getAttribute("listnexttask");
		 /*Iterator it = listnexttask.entrySet().iterator();
		 while(it.hasNext())
		 {
			    Map.Entry pair = (Map.Entry)it.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
		 }*/
		String keyGiveValue = listnexttask.get(taskName);
	   System.out.println("values : "+ keyGiveValue);
	   
	   
	   
	   String[] splitedvalue =  keyGiveValue.split("#");
	   int count = 1;
	   
	   PrintWriter out = response.getWriter();
	   for(int i = 0 ; i <splitedvalue.length; i++)
	   {
		   System.out.println(count+"). "+splitedvalue[i]+" ");
			out.print(count+"). "+splitedvalue[i]+" ");
		   count++;
	   }
		} 
		catch (Exception e)
		{
		
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		
		doGet(request, response);
	}

}
