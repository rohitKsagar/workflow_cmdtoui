package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.flowable.TaskManagement;





@WebServlet("/transition")
public class TransitionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static int countRequest = 0;
	static String process = "";
	static LinkedHashMap<String,String> hm = null;
	String chooseTask = null;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{


	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException 
	{


		countRequest++;
		System.out.println("countRequest : "+countRequest);
		TaskManagement tskmgnt = new TaskManagement();
		String cur_task ="";
		List<NextTaskPojo> listObj = new ArrayList<NextTaskPojo>();
		if(countRequest==1)
		{
			String selectedvalues = request.getParameter("selectedvalues");
			process = request.getParameter("myprocess");
			hm = new LinkedHashMap<>();
			String[] selected = selectedvalues.split(",");
			String tempString = null;
			String[] keyValueArray = null;

			for(int i =0 ; i < selected.length; i++)
			{
				tempString = selected[i];
				keyValueArray = tempString.split(":");
				for(int j =0 ; j < keyValueArray.length-1; j++)
				{
					hm.put(keyValueArray[j], keyValueArray[j+1]);

				}
			}


			try 
			{	
				HttpSession session=request.getSession();
				ServletContext sc = session.getServletContext();

				System.out.println("*********************************"+sc.getRealPath("\\WEB-INF\\classes\\"+process+".bpmn20.xml")+"*************************");
				String path= sc.getRealPath("\\WEB-INF\\classes\\"+process+".bpmn20.xml");
				
				System.out.println("Path:::"+path);
	
				GenerateBPMN gs = new GenerateBPMN();
				gs.generateXML(process,hm,path);
				


			} catch (Exception e1) 
			{

				e1.printStackTrace();
			}
			String data =null;
			try 
			{
				data = tskmgnt.taskmain(process,hm,countRequest,chooseTask);

				String[] upcmng = data.split(":");
				cur_task = upcmng[0];
				String joinedValue= upcmng[1];
				String[]up_task = joinedValue.split("#");

				for (int j = 0; j < up_task.length; j++) 
				{
					System.out.println("sSSSSS = "+up_task[j]);

					NextTaskPojo nextTaskPojoObj = new NextTaskPojo();
					String upcomingTask =up_task[j];
					nextTaskPojoObj.setNextTask(upcomingTask);
					listObj.add(nextTaskPojoObj);
				}

			}
			catch (Exception e)
			{

				e.printStackTrace();
			}
			if(data!=null)
			{
				request.setAttribute("nextTaskList", listObj);
				request.setAttribute("curtask",cur_task );	

				RequestDispatcher rd = request.getRequestDispatcher("/WorkflowTaskMangement.jsp");  
				rd.forward(request, response);
			}
		}
		else if(countRequest>1)
		{
			String endtask = request.getParameter("endTask");
			System.out.println("endtask : : " + endtask);
			chooseTask = request.getParameter("selectedRecord");
			System.out.println("chooseTask : "+chooseTask);
			String data =null;
			try 
			{
				if(endtask!=null)
				{
					chooseTask = endtask;
				}
				data = tskmgnt.taskmain(process,hm,countRequest,chooseTask);
				/*if(data=="Endtask")
				{
					request.setAttribute("nextTaskList", listObj);
					request.setAttribute("curtask",cur_task );	
					RequestDispatcher rd = request.getRequestDispatcher("/workflow.jsp");  
					rd.forward(request, response);
				}*/
				System.out.println("database : : : : :  : "+data);

				String[] upcmng = data.split(":");
				cur_task = upcmng[0];

				String joinedValue= upcmng[1];

				String[]up_task = joinedValue.split("#");

				for (int j = 0; j < up_task.length; j++) 
				{
					NextTaskPojo nextTaskPojoObj = new NextTaskPojo();
					String upcomingTask =up_task[j];
					nextTaskPojoObj.setNextTask(upcomingTask);
					listObj.add(nextTaskPojoObj);

				}

			}
			catch (Exception e)
			{

				e.printStackTrace();
			}
			if(data!=null)
			{


				request.setAttribute("nextTaskList", listObj);
				request.setAttribute("curtask",cur_task );	
				RequestDispatcher rd = request.getRequestDispatcher("/WorkflowTaskMangement.jsp");  
				rd.forward(request, response);
			}

		}



	} 

}
