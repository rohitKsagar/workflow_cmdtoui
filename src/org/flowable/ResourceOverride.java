package org.flowable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.zip.ZipInputStream;

import org.flowable.bpmn.model.BpmnModel;
import org.flowable.common.engine.api.FlowableIllegalArgumentException;
import org.flowable.common.engine.impl.util.IoUtil;

import org.flowable.engine.impl.persistence.entity.DeploymentEntity;
import org.flowable.engine.impl.persistence.entity.ResourceEntity;
import org.flowable.engine.impl.persistence.entity.ResourceEntityManager;
import org.flowable.engine.impl.util.CommandContextUtil;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;

public class ResourceOverride implements DeploymentBuilder {

	 protected transient ResourceEntityManager resourceEntityManager=CommandContextUtil.getProcessEngineConfiguration().getResourceEntityManager();
	 protected DeploymentEntity deployment=CommandContextUtil.getProcessEngineConfiguration().getDeploymentEntityManager().create();
	@Override
	public DeploymentBuilder addInputStream(String resourceName, InputStream inputStream) {
		if (inputStream == null) {
            throw new FlowableIllegalArgumentException("inputStream for resource '" + resourceName + "' is null");
        }
        byte[] bytes = IoUtil.readInputStream(inputStream, resourceName);
        
       
		ResourceEntity resource = resourceEntityManager.create();
        resource.setName(resourceName);
        resource.setBytes(bytes);
        deployment.addResource(resource);
        return this;
	}

	@Override
	public DeploymentBuilder addClasspathResource(String resource) {
		// TODO Auto-generated method stub
		 File initialFile = new File(resource);
		    InputStream inputStream= null;
			try {
				inputStream = new FileInputStream(initialFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	        if (inputStream == null) {
	            throw new FlowableIllegalArgumentException("resource '" + resource + "' not found");
	        }
	        return addInputStream(resource, inputStream);
	}

	@Override
	public DeploymentBuilder addString(String resourceName, String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder addBytes(String resourceName, byte[] bytes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder addZipInputStream(ZipInputStream zipInputStream) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder addBpmnModel(String resourceName, BpmnModel bpmnModel) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder disableSchemaValidation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder disableBpmnValidation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder name(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder category(String category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder key(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder parentDeploymentId(String parentDeploymentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder tenantId(String tenantId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder enableDuplicateFiltering() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder activateProcessDefinitionsOn(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentBuilder deploymentProperty(String propertyKey, Object propertyValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Deployment deploy() {
		// TODO Auto-generated method stub
		return null;
	}

}
