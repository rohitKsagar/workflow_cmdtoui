package org.flowable;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.flowable.bpmn.model.Activity;
import org.flowable.bpmn.model.Artifact;
import org.flowable.bpmn.model.Association;
import org.flowable.bpmn.model.BaseElement;
import org.flowable.bpmn.model.BoundaryEvent;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.ExtensionAttribute;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.Process;
import org.flowable.bpmn.model.SequenceFlow;
import org.flowable.bpmn.model.StartEvent;
import org.flowable.bpmn.model.UserTask;
import org.flowable.common.engine.api.FlowableException;
import org.flowable.engine.HistoryService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityManagerImpl;
import org.flowable.engine.impl.util.ProcessDefinitionUtil;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;


import com.util.HistoryPojo;


public class TaskManagement  {

	List<Task> tasks = null;
	TaskService taskService = null;
	Deployment deployment =null;
	static String proDefId = "";
	public String taskmain(String process, LinkedHashMap<String, String> hm, int countRequest, String chooseTask)
			throws SQLException, IOException, InterruptedException {

		TaskManagement taskManageObj = null;
		String data = null;
		taskManageObj = new TaskManagement();
		ProcessEngineConfiguration cfg =  new StandaloneProcessEngineConfiguration()
		.setJdbcUrl("jdbc:db2://localhost:50000/flowui")
		.setJdbcUsername("db2admin")
		.setJdbcPassword("db2admin")
		.setJdbcDriver("com.ibm.db2.jcc.DB2Driver")
		.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
		;

		ProcessEngine processEngine = cfg.buildProcessEngine();
		RepositoryService repositoryService = processEngine.getRepositoryService();
	
		if (countRequest == 1 || chooseTask == null) 
		{	
	
		 deployment = repositoryService.createDeployment().addClasspathResource(process+".bpmn20.xml").deploy();
		
		 
		  System.out.println("deployment : "+ deployment.getId()+ " deployment : "+deployment);
			ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
					.deploymentId(deployment.getId()).singleResult();
			proDefId =processDefinition.getId();
			
			RuntimeService runtimeService = processEngine.getRuntimeService();
			
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put(process, process);

			 ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinition.getKey(),variables);
			//ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinition.getKey());
			taskService = processEngine.getTaskService();
			
		
			/*   BpmnModel bpmnModel=repositoryService.getBpmnModel(processDefinition.getId());
			   
	          Process processBpmn = bpmnModel.getMainProcess();
	         
	       List<Association> sourceRefList =   processBpmn.findAssociationsWithSourceRefRecursive("usertask1");
	       System.out.println("sourceRefList :  : "+ sourceRefList.size());
	       for(int i = 0; i <sourceRefList.size();i++)
	       {
	    	 System.out.println( "## ## ## ## ## : "+sourceRefList.get(i)); 
	       }
	      String asd = processBpmn.getInitialFlowElement().getName();
	      System.out.println("asd asd asd asd : "+asd);
	      
	     ExecutionEntity executionEntity = 
	        BoundaryEvent boundaryEvent = (BoundaryEvent) execution.getCurrentFlowElement();
	       	
	       
	       
	       Process process1 = ProcessDefinitionUtil.getProcess(execution.getProcessDefinitionId());
	        if (process == null) {
	            throw new FlowableException("Process model (id = " + execution.getId() + ") could not be found");
	        }

	        Activity sourceActivity = null;
	        Activity compensationActivity = null;
	        List<Association> associations = process.findAssociationsWithSourceRefRecursive(boundaryEvent.getId());
	        for (Association association : associations) {
	            sourceActivity = boundaryEvent.getAttachedToRef();
	            FlowElement targetElement = process1.getFlowElement(association.getTargetRef(), true);
	            if (targetElement instanceof Activity) {
	                Activity activity = (Activity) targetElement;
	                if (activity.isForCompensation()) {
	                    compensationActivity = activity;
	                    break;
	                }
	            }
	        }
	       
	       
	      
	       
	       
	         Map<String, List<ExtensionAttribute>> mapid =processBpmn.getAttributes();
	         Iterator itq = mapid.entrySet().iterator();
		      Object[] arr=mapid.keySet().toArray();
		      while(itq.hasNext())
		      {
		    	  System.out.println("entryset : "+ itq.next());
		      }*/
	         /*Activity activity = (Activity) processBpmn.findAssociationsWithSourceRefRecursive(sourceRef)
	            List<SequenceFlow> outgoingFlows = activity.getOutgoingFlows();  
	            for(SequenceFlow b :outgoingFlows)
			       {  
		        	String  getSourceRef= b.getSourceRef();
				   	System.out.println("getSourceRef getSourceRef getSourceRef :"+ getSourceRef  ); 
			 
			   	    } */
	          /*List<Association> asso = processBpmn.findAssociationsWithSourceRefRecursive("usertask1");
	            System.out.println("hope : : aas.size() "+asso.size());
	            for (Association assoss : asso) 
	            {
				  System.out.println("assoss.getSourceRef()  : "+ assoss.getSourceRef() +"assoss.getTargetRef() :"+assoss.getTargetRef());
				  
				}
			*/
			
			
			
		}
		if (countRequest == 1) {

			data = taskManageObj.openTask(taskService, hm);

		}
		if (chooseTask != null) {

			taskManageObj = new TaskManagement();
			taskService = processEngine.getTaskService();
			data = taskManageObj.RemainingTask(taskService, hm, countRequest, chooseTask);
		}
		return data;
	}

	String openTask(TaskService taskService, LinkedHashMap<String, String> hm) {

		
		
		System.out.println("Process Defination Id : "+proDefId);
		tasks = taskService.createTaskQuery().processDefinitionId(proDefId).active().list();
		for(Task ts : tasks)
		{
			System.out.println("Task name by proDefId :"+ts.getName() );
		}
		
		
		String upcomingTask = "";
		upcomingTask += tasks.get(tasks.size() - 1).getName() + ":";
		String keyGivenValue = hm.get(tasks.get(tasks.size() - 1).getName());
		System.out.println("checking old value or not : : :  "+ tasks.get(tasks.size() - 1).getName());
		String[] splitedvalue = keyGivenValue.split("#");

		for (int i = 0; i < splitedvalue.length; i++)
		{
			if (i != 0) 
			{
				upcomingTask += "#";
			}

			upcomingTask += splitedvalue[i];

		}

		return upcomingTask;
	}

	public String RemainingTask(TaskService taskService, LinkedHashMap<String, String> hm, int countRequest,
			String chooseTask) {

		tasks = taskService.createTaskQuery().processDefinitionId(proDefId).active().list();
		for(Task ts : tasks)
		{
			System.out.println("Task name by proDefId :"+ts.getName());
		}
		String upcomingTask = "";
		upcomingTask += chooseTask + ":";
		String keyGivenValue = hm.get(chooseTask);
		String[] splitedvalue = keyGivenValue.split("#");
	
		for (int i = 0; i < splitedvalue.length; i++)
		{
			if (i != 0) 
			{
				upcomingTask += "#";
			}
			upcomingTask += splitedvalue[i];
			

		}
		Map<String, Object> approvedVariables = new HashMap<String, Object>();
		approvedVariables.put("input", chooseTask);
		System.out.println("tasks.get(tasks.size() - 1).getId() : "+tasks.get(tasks.size() - 1).getId());
		
		taskService.complete(tasks.get(tasks.size() - 1).getId(), approvedVariables);
		
	//	EmailClass.sendEmail(tasks.get(tasks.size() - 1).getName());
		return upcomingTask;
	}

	public List history(String Processname) throws SQLException 
	{
		ProcessEngineConfiguration cfg =  new StandaloneProcessEngineConfiguration()
		.setJdbcUrl("jdbc:db2://localhost:50000/flowui")
		.setJdbcUsername("db2admin")
		.setJdbcPassword("db2admin")
		.setJdbcDriver("com.ibm.db2.jcc.DB2Driver")
		.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
		;

		ProcessEngine processEngine = cfg.buildProcessEngine();
		Connection ds = cfg.getDataSource().getConnection();

		String sql = "SELECT NAME_ FROM act_hi_taskinst WHERE PROC_INST_ID_ IN (SELECT PROC_INST_ID_ FROM act_hi_varinst WHERE TEXT_= ?)";

		PreparedStatement ps = ds.prepareStatement(sql);
		ps.setString(1, Processname);
		ResultSet rs = ps.executeQuery();
		List histroyList = new LinkedList<>();
		while (rs.next()) {
			HistoryPojo hisPojo = new HistoryPojo(rs.getString(1));
			histroyList.add(hisPojo);

		}
		return histroyList;
	}

}